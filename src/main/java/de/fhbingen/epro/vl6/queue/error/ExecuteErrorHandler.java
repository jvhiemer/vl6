package de.fhbingen.epro.vl6.queue.error;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

import de.fhbingen.epro.vl6.model.Test;

/**
 * 
 * @author Johannes Hiemer.
 *
 */
@Component
public class ExecuteErrorHandler implements ErrorHandler {
	
	protected static final Logger log = LoggerFactory
			.getLogger(ExecuteErrorHandler.class);
	
	/* (non-Javadoc)
	 * @see org.springframework.util.ErrorHandler#handleError(java.lang.Throwable)
	 */
	public void handleError(Test test, Throwable t) {
		StringWriter stringWriter = new StringWriter();
		t.printStackTrace(new PrintWriter(stringWriter));
		
		String exceptionAsString = stringWriter.toString();
		
		log.info("Exception thrown with: {}", exceptionAsString);
	}

	public void handleError(Throwable t) {
		StringWriter stringWriter = new StringWriter();
		t.printStackTrace(new PrintWriter(stringWriter));
		
		String exceptionAsString = stringWriter.toString();
		
		log.info("Exception thrown with: {}", exceptionAsString);
	}

}