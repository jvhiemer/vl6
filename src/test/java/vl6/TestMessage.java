/**
 * 
 */
package vl6;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.fhbingen.epro.vl6.BaseConfiguration;

/**
 * @author Johannes Hiemer.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { BaseConfiguration.class })
public class TestMessage {
	
	@Autowired
	RabbitTemplate rabbitTemplate;

	@Test
	public void test() {
		de.fhbingen.epro.vl6.model.Test test = new 
				de.fhbingen.epro.vl6.model.Test(new Date().toString(), "This is another sample comment at: " + new Date().toString());
		rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
		rabbitTemplate.convertAndSend("demo.exchange", "demo.rtg.key", test);
		
	}

}
